import { container } from "assets/jss/nextjs-material-kit.js";

const carouselStyle = {
  section: {
    padding: "70px 0"
  },
  container,
  marginAuto: {
    // marginLeft: "auto !important",
    // marginRight: "auto !important",
    marginTop: "-2%",
    maxWidth:'100%',
    height:'430px'
  }
};

export default carouselStyle;
